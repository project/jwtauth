<?php

namespace Drupal\jwtauth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure JSON Web Token (JWT) Authentication settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  protected $config;
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jwtauth_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jwtauth.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['signing_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('JWT Signing Key'),
      '#required' => true,
      '#default_value' => $this->config('jwtauth.settings')->get('signing_key'),
    ];

    $form['jwa'] = [
      '#type' => 'select',
      '#title' => $this->t('Algorithm (JWA)'),
      '#required' => true,
      '#options' => [
        'HS256' => 'HS256',
        'RS256' => 'RS256',
        'RS512' => 'RS512',
      ],
      '#default_value' => $this->config('jwtauth.settings')->get('jwa'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('jwtauth.settings')
      ->set('signing_key', $form_state->getValue('signing_key'))
      ->set('jwa', $form_state->getValue('jwa'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
