<?php

namespace Drupal\jwtauth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManager;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Firebase\JWT\JWT;

/**
 * Returns responses for JSON Web Token (JWT) Authentication routes.
 */
class JWTAuthenticateController extends ControllerBase
{

  private $config;
  public $externalAuth;

  public function __construct(ExternalAuthInterface $externalauth)
  {
    $this->externalAuth = $externalauth;
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('externalauth.externalauth')
    );
  }

  public function authenticate($jwt)
  {
    $key_id = $this->_get_config('signing_key');
    $key = $this->_get_key();
    $jwa = $this->_get_config('jwa') || 'RS256';

    $this->getLogger('jwtauth')->notice(
      "User attempting login with JWT token (@jwt)",
      ['@jwt' => $jwt]
    );

    $payload = (array)JWT::decode($jwt, $key, [$jwa]);

    if (empty($payload['kid']) || $payload['kid'] !== $key_id) {
      $this
        ->getLogger('jwtauth')
        ->warning('Unsuccessful log-in due to mis-matched KID (@kid1 vs @kid2)', [
          '@kid1' => empty($payload['kid']) ? null : $payload['kid'],
          '@kid2' => $key_id,
        ]);
      throw new NotFoundHttpException('KID mis-match');
    }

    $subject = $payload['sub'] ?? $payload['subject'];
    $authname = $subject;
    $uid = (int)str_replace('urn:uid:', '', $subject);

    $this
      ->getLogger('jwtauth')
      ->notice("Logging in user URN @subject", [
        '@subject' => $subject
      ]);

    $account = $this->externalAuth->load($authname, 'jwtauth');

    if (!$account) {
      $this
        ->getLogger('jwtauth:authenticate')
        ->notice('@authname not found in externalauth. Trying to register.', [
          '@authname' => $authname
        ]);

      // Fetch user account from DB
      $account = User::load($uid);

      if (!$account) {
        $this
          ->getLogger('jwtauth')
          ->warning("Cannot find user with ID @uid.", [
            '@uid' => $uid,
          ]);
        throw new NotFoundHttpException('Invalid User');
      }

      $this->externalAuth->linkExistingAccount($authname, 'jwtauth', $account);

      $this
        ->getLogger('jwtauth')
        ->notice('Logging user @uid in with @authname', [
          '@authname' => $authname,
          '@uid' => $uid,
        ]);
    }

    if ($account) {
      $this->externalAuth->userLoginFinalize($account, $authname, 'jwtauth');
      return new RedirectResponse('/user', RedirectResponse::HTTP_FOUND);
    }

    return new RedirectResponse('/user/login');
  }

  private function _get_config($key)
  {
    if (!$this->config) {
      $this->config = \Drupal::config('jwtauth.settings');
    }
    return $this->config->get($key);
  }

  private function _get_key()
  {
    $key_id = $this->_get_config('signing_key');
    $key = \Drupal::service('key.repository')->getKey($key_id)->getKeyValue();

    if (str_starts_with($key, '-----BEGIN RSA PRIVATE KEY-----')) {
      $key = openssl_pkey_get_private($key);
      $pem_public_key = openssl_pkey_get_details($key)['key'];
      return openssl_pkey_get_public($pem_public_key);
    }
    return $key;
  }
}
